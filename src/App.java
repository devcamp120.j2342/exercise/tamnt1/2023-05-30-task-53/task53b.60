
import model.Mammal;
import model.Animal;
import model.Cat;
import model.Dog;

public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("Lion");
        Animal animal2 = new Animal("Bird");
        System.out.println(animal1);
        System.out.println(animal2);

        Mammal mammal1 = new Mammal("Tiger");
        Mammal mammal2 = new Mammal("Hippo");
        System.out.println(mammal1);
        System.out.println(mammal2);
        Cat cat1 = new Cat("Zin");
        Cat cat2 = new Cat("Bum");
        cat1.greet();
        System.out.println(cat1);
        System.out.println(cat2);
        Dog dog1 = new Dog("Sun");
        Dog dog2 = new Dog("Sam");
        dog1.greet();
        dog1.greets(dog2);
        System.out.println(dog1);
        System.out.println(dog2);
    }
}
